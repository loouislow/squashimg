//
// Krunch Timer
//
// @@script: timer.js
// @@description: task configuration for schedule
// @@version:
// @@author: Loouis Low
// @@copyright: Goody Technologies
//

var cron = require('node-cron'),
    shell = require('shelljs');

// ┌────────────── second (optional)
// │ ┌──────────── minute
// │ │ ┌────────── hour
// │ │ │ ┌──────── day of month
// │ │ │ │ ┌────── month
// │ │ │ │ │ ┌──── day of week
// │ │ │ │ │ │
// │ │ │ │ │ │
// * * * * * *

//testing mode is 1 minute
cron.schedule('* * * * *', function(){
  console.log('[squashimg] is running...');
  //job
  if (shell.exec('/bin/bash init.sh').code !== 0) {
    shell.echo('[squashimg] Error: execution failed.');
    shell.exit(1);
  }
});
