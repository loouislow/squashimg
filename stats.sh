#!/bin/bash
#
# Krunch
#
# @@script: stats.sh
# @@description: assets statistics
# @@version:
# @@author: Loouis Low
# @@copyright: Goody Technologies
#

function stats() {
  clear
  echo ""
  echo -e "\e[94m[SquashImg] Image Statistics : source/"
  echo ""
  grunt --gruntfile grunt-tasks/grunt.task.stats.source.js
  echo ""
}

### init
stats
