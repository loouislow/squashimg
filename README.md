## SquashImg

A lightweight image optimizer built with `Krunch` tool.

###### How does it work?

```text
Image Folder1 ---> | Symbolic-link |      | Grunt  |
Image Folder2 ---> | to /source/   | ---> | Tasks  | ---> (Optimize) + (Generate Statistics)
Image Folder3 ---> | folder        |      | Runner |
```

#### Step 1 : Symbolic-link Image folders

Using symlink to map the folder from one or multiple folders that containing images to **SquashImg's** `/source/` folder as example. Symlink uses full-path, add **$PWD** before the directive.

```bash
$ ln -s $PWD/<image_folder> $PWD/SquashImg/source/
```

#### Step 2 : Setup A New Task

There are **2** extra steps to complete a New Task set up, which are you need to:

**One:** Create a new task file at `grunt-task` folder. But, you can use the **__spare** example task files. Duplicating and edit.

```text
grunt.task.optimize.img.02.js__spare
```

you need to rename the filename to,

```text
grunt.task.optimize.img.02.js
```

**Two:** Register the new grunt-tasks filename to `builder.sh` located at `bash-tasks` folder.

**Be sure to check the filename is correct.**

```bash
# -------Example-------
echo ""
echo -e "\e[94m[SquashImg] optimizing : Images Source 1"
echo ""
grunt --gruntfile ${TX}/grunt.task.optimize.img.02.js
```
