//
// Krunch Tasker
//
// @@script: grunt.task.optimize.img.01.js
// @@description: task configuration for Compressing Images
// @@version:
// @@author: Loouis Low
// @@copyright: Goody Technologies
//

/***
** Configurations
*/

module.exports = function(grunt) {
	grunt.initConfig({
		pkg: grunt.file.readJSON('../package.json'),
		// Images Compressor
		imagemin: {
			options: {
				// Default: false
				cache: true,
				// Default: 1 (0-7)
				optimizationLevel: 1
			},
			dist: {
				files: [{
					expand: true,
					// target assets
					cwd: '../source/',
					// find all image files
					src: ['**/*.{png,jpg,gif}'],
					// optimized assets move to (overwrite)
					dest: '../source/'
				}]
			}
		}
	});

	/***
	** Run tasks
	*/

	grunt.loadNpmTasks('grunt-contrib-imagemin');
	grunt.registerTask('default', ['imagemin']);

	// results
	require('time-grunt')(grunt);
};
