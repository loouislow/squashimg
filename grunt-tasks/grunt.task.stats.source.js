//
// Krunch Tasker
//
// @@script: grunt.task.stats.source.js
// @@description: task configuration for Statistics - source/
// @@version:
// @@author: Loouis Low
// @@copyright: Goody Technologies
//

/***
** Configurations
*/

module.exports = function(grunt) {
	grunt.initConfig({
		pkg: grunt.file.readJSON('../package.json'),
		// Project Statistics
		stats: {
			src_dir: ['../source/']
			//custom_options: {
				//options: {
				//	mode: 'verbose'
				//},
				// folder to analyze
				//src: ['dist/']
			//}
		},
	});

	/***
	** Run tasks
	*/

	grunt.loadNpmTasks('grunt-stats');
	grunt.registerTask('default', ['stats']);
};
