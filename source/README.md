# Symlink Image Source Folder

###### How does it work?

```text
Image Folder1 ---> | Symbolic-link |      | Grunt  |
Image Folder2 ---> | to /source/   | ---> | Tasks  | ---> (Optimize) + (Generate Statistics)
Image Folder3 ---> | folder        |      | Runner |
```

### How do I optimize the Image Source?

**Step 1:** Create a new symbolic-link where the image folders you want to optimize, and `symlink` them into the `/source/` folder. Symlink uses full-path directive, add `$PWD`.

```bash
$ ln -s $PWD/<image target folder> $PWD/<destination folder /source/>
```

### Can I add Image Source from multiple folder locations?

**Step 1:** Yes, you can. To do so, duplicate a new tasks file located at `/grunt-tasks/` folder. Or use the `spare file` to duplicate.

**Step 2:** Add new line `grunt-tasks filename` on `builder.sh` file, located at `/bash-tasks` folder.

```bash
# ------- Example-------
echo ""
echo -e "\e[94m[SquashImg] optimizing : Images Source 1"
echo ""
grunt --gruntfile ${TX}/grunt.task.optimize.img.01.js
```
