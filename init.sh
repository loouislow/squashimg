#!/bin/bash
#
# Krunch
#
# @@script: build.sh
# @@description: krunch build-tool
# @@version:
# @@author: Loouis Low
# @@copyright: Goody Technologies
#

BT="bash-tasks"

### include bash tasks
source ${BT}/header.sh
source ${BT}/builder.sh

#######
### init
#######

#
# SHOW HEADER
# Customize header information to represent project subject or title.
#
# ** enabled by (Default)
###
show_header

#
# BUILDER
# A set of Grunt task files will running when this build.sh is executed.
# Check grunt-tasks/ folder for more details.
#
# ** enabled by (Default)
###
builder
