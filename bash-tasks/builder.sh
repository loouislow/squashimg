#!/bin/bash

TX="$PWD/grunt-tasks"

function builder() {
	# -------Image Source 1-------
	echo ""
	echo -e "\e[94m[SquashImg] optimizing : Images Source 1"
	echo ""
	grunt --gruntfile ${TX}/grunt.task.optimize.img.01.js

	# Uncomment the spare tasks, if you need more task runner.
	# Also, rename and remove the __spare from the filename
	# located at folder "grunt-tasks".
	#
	# Example,
	#
	# grunt.tasks.optimize.img02.js__spare (rename to)
	# grunt.tasks.optimize.img02.js (this one!)

	# -------Image Source 2-------
	# echo ""
	# echo -e "\e[94m[SquashImg] optimizing : Images Source 2"
	# echo ""
	# grunt --gruntfile ${TX}/grunt.task.optimize.img.02.js

	# -------Image Source 3-------
	# echo ""
	# echo -e "\e[94m[SquashImg] optimizing : Images Source 3"
	# echo ""
	# grunt --gruntfile ${TX}/grunt.task.optimize.img.03.js
}
