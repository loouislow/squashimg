#!/bin/bash

function show_header() {
	clear
	echo -e "\e[94m-----------------------------------------------------"
	echo -e "\e[94m SquashImg, passive image optimizer seamlessly"
	echo -e "\e[94m-----------------------------------------------------"
}
